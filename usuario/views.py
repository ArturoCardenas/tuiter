from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth

def home(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/perfil/')
	return render_to_response('home.html')

def login(request):
	usr = request.POST.get('user')
	pwd = request.POST.get('password')
	if not usr or not pwd:
		return HttpResponseRedirect('/')
	user = auth.authenticate(username = usr, password = pwd)
	if user is None:
		return HttpResponseRedirect('/')
	auth.login(request, user)
	return HttpResponseRedirect('/perfil/')

def logout(request):
	auth.logout(request)
	return HttpResponseRedirect('/')

