'use strict';

var jelou =
{
	'init': function()
	{
		/*var columna_contenido = $('#boton-ajax');
		columna_contenido.on('click', jelou.traerAlumnos);*/
		$('#boton-ajax').on('click', function() {
			jQuery.ajax({
				'url': '/alumnos/',
				'success': function(respuesta) {
					$('#columna-contenido').html(respuesta);
				}
			});
		});
	}

	/*'traerAlumnos': function()
	{
		jQuery.ajax({
			'url': '/alumnos/',
			'success': function(respuesta) {
				$('#columna-contenido').html(respuesta);
			}
		});
	}*/
};

$(window).on('load', jelou.init);
